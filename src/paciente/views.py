from django.shortcuts import render
from .forms import RegForm
from .models import Paciente

# Create your views here.
def inicio(request):
    form = RegForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        nombre2 = form_data.get("nombre")
        rut2 = form_data.get("rut")
        objeto = Paciente.objects.create(nombre=nombre2, rut=rut2)

    contexto = {
            "cualquiercosa": form,
        }
    return render(request, "inicio.html", contexto)
