from django.db import models

# Create your models here.
class Paciente(models.Model):
    nombre = models.CharField(max_length=100, blank=False, null=False)
    rut = models.CharField(max_length=10, blank=False, null=False)
    fecha_ingreso = models.DateField(auto_now_add=True, auto_now=False)
    #fecha_ingreso = models.DateField(auto_now_add=False, auto_now=False)

    def __str__(self):
        return self.nombre




