from django import forms
from .models import Paciente


class RegForm(forms.Form): # RegForm es el nombre para el registro del formulario
    nombre = forms.CharField(max_length=100)
    rut = forms.CharField(max_length=10)

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre: # Validamos que el contenido del campo nombre no venga vacío
            if len(nombre) <= 1: # Verificamos que el contenido del campo nombre tiene un caracter
                raise forms.ValidationError("El campo nombre no puede ser de un caracter, NOMBRE NO VALIDO!")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")


class RegModelForm(forms.ModelForm):
    class Meta:
        modelo = Paciente
        campos = ["nombre", "rut"]

    def clean_nombre(self):
        nombre = self.cleaned_data.get("nombre")
        if nombre: # Validamos que el contenido del campo nombre no venga vacío
            if len(nombre) <= 1: # Verificamos que el contenido del campo nombre tiene un caracter
                raise forms.ValidationError("El campo nombre no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")
