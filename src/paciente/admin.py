from django.contrib import admin
from .models import Paciente
from .forms import RegModelForm

class AdminPaciente(admin.ModelAdmin):
    model = RegModelForm
    list_display = ["nombre", "rut", "fecha_ingreso"]
    list_filter = ["rut"]
    search_fields = ["rut", "nombre"]
    #list_editable = ["rut"]


    # class Meta:
    #     model = Paciente

# Register your models here.
#admin.site.register(AdminPaciente)
admin.site.register(Paciente, AdminPaciente)

