from django.contrib import admin
from .models import Especialistas
# from .forms import RegModelForm2

class AdminEspecialistas(admin.ModelAdmin):
    # model = RegModelForm2
    list_display = ["nombre", "email"]
    list_filter = ["email"]
    search_fields = ["email", "nombre"]
    #list_editable = ["rut"]

    class Meta:
        model = Especialistas

# Register your models here.
#admin.site.register(AdminEspecialistas)
admin.site.register(Especialistas, AdminEspecialistas)
