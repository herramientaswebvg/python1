from django.shortcuts import render
from .forms import RegFormE
from .models import Especialistas

# Create your views here.
def ingreso_e(request):
    form = RegFormE(request.POST or None)

    if form.is_valid():
        form_data = form.cleaned_data
        nombre = form_data.get("nombre")
        email = form_data.get("email")
        objeto = Especialistas.objects.create(nombre=nombre, email=email)

    contexto = {
        "formulario_ingreso_e": form,
    }
    return render(request, "ingreso_e.html", contexto)
